package utils;

public class NotImplementedException extends RuntimeException {

    private static final long serialVersionUID = 8645275528322929203L;
    
    public NotImplementedException() {
        super("Not yet implemented.");
    }

}
