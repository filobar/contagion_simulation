package utils;

import utils.size.Sized;
import utils.size.Types;

public final class Point2D extends Pair<Double, Double> implements Sized {
    
    public Point2D(final double x, final double y) {
        super(x,y);
    }
    
    public static Point2D random() {
        return new Point2D(Math.random(), Math.random());
    }
    
    public static Point2D random(final double minX, final double maxX, final double minY, final double maxY) {
        return new Point2D(random(minX, maxX), random(minY, maxX));
    }
    
    //TODO check this method
    private static double random(final double min, final double max) {
        return Math.random()*(max-min) + min;
    }
    
    public static Point2D add(final Point2D p1, final Point2D p2) {
        return new Point2D(p1.x+p2.x, p1.y+p2.y);
    }
    
    public static double dist(final Point2D p1, final Point2D p2) {
        return Math.sqrt(square(p2.x-p1.x) + square(p2.y-p1.y));
    }
    
    //TODO check this method
    private static double square(final double x) {
        return x*x;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((x == null) ? 0 : x.hashCode());
        result = prime * result + ((y == null) ? 0 : y.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Point2D other = (Point2D) obj;
        if (x == null) {
            if (other.x != null)
                return false;
        } else if (!x.equals(other.x))
            return false;
        if (y == null) {
            if (other.y != null)
                return false;
        } else if (!y.equals(other.y))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Point2D [x=" + x + ", y=" + y + "]";
    }

    @Override
    public int getSize() {
        return 2*Types.DOUBLE_SIZE;
    }

}
