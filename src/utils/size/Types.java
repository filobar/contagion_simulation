package utils.size;

/**
 * A class to calculate a precise value of memory allocated.
 */
public final class Types {
    
    //Base values
    public static final int BYTE_SIZE = Byte.BYTES;
    public static final int SHORT_SIZE = Short.BYTES;
    public static final int INT_SIZE = Integer.BYTES;
    public static final int LONG_SIZE = Long.BYTES;
    public static final int FLOAT_SIZE = Float.BYTES;
    public static final int DOUBLE_SIZE = Double.BYTES;
    public static final int CHAR_SIZE = Character.BYTES;
    public static final int BOOL_SIZE = 1;
    
    public static int sizeof(String s) {
        return s.length() * CHAR_SIZE;
    }

}
