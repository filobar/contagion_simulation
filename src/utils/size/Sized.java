package utils.size;

/**
 * An Object implementing this interface exposes a method to know
 * at any time an approximation of its memory usage expressed as
 * number of bytes allocated.
 */
public interface Sized {
    
    /**
     * @return
     *          The memory currently used by the Object expressed
     *          as number of bytes allocated.
     */
    int getSize();

}
