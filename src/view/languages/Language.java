package view.languages;

/**
 * Contains all available languages in form of enum values.
 */
public enum Language {
    
    ITALIAN,
    ENGLISH

}
