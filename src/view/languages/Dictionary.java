package view.languages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Manages all 'printable' words
 * in all available languages.
 * 
 * With 'Singleton' pattern
 */
public final class Dictionary {
    
    //Static pointer to only existing instance
    private static Dictionary d = null;
    
    private final String folder = "lang";
    
    //Map where words are actually stored
    private final Map<String, String> words = new HashMap<>();
    
    //Language currently set. Default = English
    private Language currentLanguage;
    
    private String currentPath;
    
    private Dictionary() {
        super();
        
        assertExistingFolder("res");
        assertExistingFolder("res" + File.separator + this.folder);
        
        for(Language lang : Language.values()) {
            assertExistingFile("res" + File.separator + this.folder + File.separator + lang.name().toLowerCase() + ".txt");
        }
        
        fillFiles();
        
        setCurrentLanguage(Language.ENGLISH);
    }
    
    private void assertExistingFolder(final String folderPath) {
        final Path path = Paths.get(folderPath);
        if(!Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
            new File(path.toString()).mkdir();
        }
    }
    
    private void assertExistingFile(final String filePath) {
        final Path path = Paths.get(filePath);
        if(!Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
            try {
                new File(path.toString()).createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void fillFiles() {
        //ENGLISH
        try(FileWriter fw = new FileWriter(new File("res" + File.separator + this.folder + File.separator + 
                Language.ENGLISH.name().toLowerCase() + ".txt"))) {
            
            fw.write("\"version\"=\"Version\"\n");
            fw.write("\"play\"=\"Play\"\n");
            fw.write("\"exit\"=\"Exit\"\n");
            fw.write("\"settings\"=\"Settings\"\n");
            fw.write("\"change language\"=\"Change Language\"\n");
            fw.write("\"back\"=\"Back\"\n");
            fw.write("\"next day\"=\"Next Day\"\n");
            fw.write("\"params\"=\"Parameters\"\n");
            fw.write("\"ready\"=\"Ready\"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        //ITALIAN
        try(FileWriter fw = new FileWriter(new File("res" + File.separator + this.folder + File.separator + 
                Language.ITALIAN.name().toLowerCase() + ".txt"))) {
            
            fw.write("\"version\"=\"Versione\"\n");
            fw.write("\"play\"=\"Gioca\"\n");
            fw.write("\"exit\"=\"Esci\"\n");
            fw.write("\"settings\"=\"Impostazioni\"\n");
            fw.write("\"change language\"=\"Cambia Lingua\"\n");
            fw.write("\"back\"=\"Indietro\"\n");
            fw.write("\"next day\"=\"Prossimo Giorno\"\n");
            fw.write("\"params\"=\"Parametri\"\n");
            fw.write("\"ready\"=\"Pronto\"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * Dictionary is implemented using the Singleton pattern.
     * When called the very first time, this method instantiates
     * a Dictionary, so it might take some time.
     * 
     * @return
     *          The only existing instance of Dictionary.
     */
    public static Dictionary getInstance() {
        if(Dictionary.d == null) {
            Dictionary.d = new Dictionary();
        }
        return Dictionary.d;
    }
    
    public Language getCurrentLanguage() {
        return this.currentLanguage;
    }
    
    /**
     * Sets the new current {@link Language},
     * clears the internal map of words
     * and refills it reading from the corresponding file.
     * (For these reasons, a single call to this method
     * might last for a long time)
     * 
     * @param newLanguage
     *          The new language to be set.
     */
    public void setCurrentLanguage(final Language newLanguage) {
        if(this.currentLanguage != newLanguage) {
            this.currentLanguage = Objects.requireNonNull(newLanguage);
            this.words.clear();
            this.currentPath = "res" + File.separator + this.folder + File.separator + 
                                this.currentLanguage.name().toLowerCase() + ".txt";
            final File f = new File(this.currentPath);
    
            if(!f.exists()) {
                throw new IllegalStateException("Couldn't find file for " + this.currentLanguage.name());
            }
            
            try (BufferedReader br = new BufferedReader(new FileReader(f))){
                br.lines().forEach(line -> {
                    String[] strarr = line.split("\"");
                    this.words.put(strarr[1], strarr[3]);
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * @param wordRequired
     *          The String corresponding to 
     * @return
     */
    public String getWordFor(final String wordRequired) {
        Objects.requireNonNull(wordRequired);
        
        //Asserting given word is contained
        if(!this.words.containsKey(wordRequired)) {
            throw new IllegalStateException(wordRequired + " not set in Dictionary.");
        }
        
        return this.words.get(wordRequired);
    }

}
