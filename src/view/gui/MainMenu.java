package view.gui;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import view.languages.Dictionary;

public final class MainMenu extends GUI {

    private static final long serialVersionUID = 510381758709866267L;
    private static final String versionNumber = "0.1";
    private final Dictionary dictionary = Dictionary.getInstance();
    
    public MainMenu() {
        super();
        
        //Main panel
        final JPanel mainPanel = new JPanel(new BorderLayout());
        this.add(mainPanel);
        
        //Title
        final JLabel titleLabel = new JLabel("Contagion Simulation", SwingConstants.CENTER);
        mainPanel.add(titleLabel, BorderLayout.NORTH);
        
        //Center Panel
        final JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        
        //'Play' button
        final JButton play = new JButton(this.dictionary.getWordFor("play"));
        centerPanel.add(play);
        play.addActionListener(e -> {
            this.load(new Parameters());
        });
        
        //'Settings' button
        final JButton settings = new JButton(this.dictionary.getWordFor("settings"));
        centerPanel.add(settings);
        settings.addActionListener(e -> {
            this.load(new Settings());
        });
        
        //'Exit' button
        final JButton exit = new JButton(this.dictionary.getWordFor("exit"));
        centerPanel.add(exit);
        exit.addActionListener(e -> {
            System.exit(0);
        });
        
        //Version label
        final JLabel versionLabel = new JLabel(this.dictionary.getWordFor("version") + " " + versionNumber, SwingConstants.RIGHT);
        mainPanel.add(versionLabel, BorderLayout.SOUTH);
        
        this.setVisible(true);
        this.pack();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
