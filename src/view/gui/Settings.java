package view.gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import view.languages.Dictionary;
import view.languages.Language;

public class Settings extends GUI {

    private static final long serialVersionUID = -9009510796844370824L;
    private final Dictionary dictionary = Dictionary.getInstance();
    
    public Settings() {
        super();
        
        //Main panel
        final JPanel mainPanel = new JPanel(new BorderLayout());
        this.add(mainPanel);
        
        //Title label
        final JLabel title = new JLabel(this.dictionary.getWordFor("settings"), SwingConstants.CENTER);
        mainPanel.add(title, BorderLayout.NORTH);
        
        //'Change language' combo box
        final JComboBox<Language> changeLanguage = new JComboBox<>(Language.values());
        changeLanguage.setSelectedItem(this.dictionary.getCurrentLanguage());
        mainPanel.add(changeLanguage, BorderLayout.CENTER);
        
        //'Back' button
        final JButton back = new JButton(this.dictionary.getWordFor("back"));
        mainPanel.add(back, BorderLayout.SOUTH);
        back.addActionListener(e -> {
            this.dictionary.setCurrentLanguage((Language)changeLanguage.getSelectedItem());
            this.load(new MainMenu());
        });
        
        this.pack();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
