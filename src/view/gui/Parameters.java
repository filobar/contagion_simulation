package view.gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import view.languages.Dictionary;

public final class Parameters extends GUI {

    private static final long serialVersionUID = 7752611514340268951L;
    private final Dictionary dictionary = Dictionary.getInstance();
    
    public Parameters() {
        super();
        
        //Main Panel
        final JPanel mainPanel = new JPanel(new BorderLayout());
        this.add(mainPanel);
        
        //Title label
        final JLabel title = new JLabel(this.dictionary.getWordFor("params"), SwingConstants.CENTER);
        mainPanel.add(title, BorderLayout.NORTH);
        
        //'Ready' button
        final JButton ready = new JButton(this.dictionary.getWordFor("ready"));
        mainPanel.add(ready, BorderLayout.CENTER);
        ready.addActionListener(e -> {
            this.load(new Game());
        });
        
        //'Back' button
        final JButton back = new JButton(this.dictionary.getWordFor("back"));
        mainPanel.add(back, BorderLayout.SOUTH);
        back.addActionListener(e -> {
            this.load(new MainMenu());
        });
        
        this.pack();
    }

}
