package view.gui;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import model.simulation.Simulation;
import view.languages.Dictionary;

//TODO change this name
public class Game extends GUI {

    private static final long serialVersionUID = 8833756580473667340L;
    private final Simulation sim = this.controller.getNewSimulation();
    private final Dictionary dictionary = Dictionary.getInstance();
    private final JLabel memory = new JLabel();
    
    public Game() {
        super();
        
        //Main panel
        final JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        this.add(mainPanel);
        
        //Title label
        final JLabel title = new JLabel("Simulation", SwingConstants.CENTER);
        mainPanel.add(title);
        
        //Labels panel
        final JPanel labelsPanel = new JPanel();
        labelsPanel.setLayout(new BoxLayout(labelsPanel, BoxLayout.Y_AXIS));
        mainPanel.add(labelsPanel);
        
        //Current day label
        final JLabel day = new JLabel(Integer.toString(this.sim.getCurrentDay()));
        labelsPanel.add(day);
        
        //Total population label
        final JLabel population = new JLabel(this.sim.getWorld().getInfected().toString());
        labelsPanel.add(population);
        
        //Live memory label
        labelsPanel.add(memory);
        this.updateMemoryLabel();
        
        //'Next day' button
        final JButton next = new JButton(this.dictionary.getWordFor("next day"));
        mainPanel.add(next);
        next.addActionListener(e -> {
            this.sim.nextDay();
            population.setText(this.sim.getWorld().getInfected().toString());
            day.setText(Integer.toString(this.sim.getCurrentDay()));
            updateMemoryLabel();
        });
        
        //'Back' button
        final JButton back = new JButton(this.dictionary.getWordFor("back"));
        mainPanel.add(back);
        back.addActionListener(e -> {
            this.load(new MainMenu());
        });
        
        this.pack();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    private void updateMemoryLabel() {
        this.memory.setText(this.sim.getSize() + " bytes");
    }

}
