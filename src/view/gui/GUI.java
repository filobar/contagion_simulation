package view.gui;

import javax.swing.JFrame;

import controller.Controller;
import controller.ControllerImpl;

public abstract class GUI extends JFrame {

    private static final long serialVersionUID = -6240313924033756724L;
    protected final Controller controller = ControllerImpl.getInstance();
    
    protected void load(final GUI gui) {
        this.setVisible(false);
        gui.setVisible(true);
        gui.setLocation(this.getLocation());
    }

}
