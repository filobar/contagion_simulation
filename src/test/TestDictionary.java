package test;

import static org.junit.Assert.fail;

import org.junit.Test;

import view.languages.Dictionary;

public class TestDictionary {
    
    private final Dictionary d = Dictionary.getInstance();
    
    @Test(expected = NullPointerException.class)
    public void cantPassNull() {
        this.d.getWordFor(null);
        fail("Null shouldn't be passed to dictionary.");
    }
    
    @Test(expected = IllegalStateException.class)
    public void wordNotContained() {
        this.d.getWordFor("ciccio barlocco");
        fail("An exception should be thrown when word isn't contained.");
    }
    
    //TODO

}
