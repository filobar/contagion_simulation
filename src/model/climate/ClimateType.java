package model.climate;

public enum ClimateType {
    
    COLD,
    TEMPERATE,
    HOT

}
