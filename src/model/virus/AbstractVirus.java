package model.virus;

import utils.size.Types;

public abstract class AbstractVirus implements Virus {
    
    protected final double infectionRate;
    protected final double mortalityRate;
    protected final double healingRate;
    
    protected AbstractVirus(final double infection, final double mortality, final double healing) {
        this.infectionRate = infection;
        this.mortalityRate = mortality;
        this.healingRate = healing;
    }
    
    public double getInfectionRate() {
        return this.infectionRate;
    }

    public double getMortalityRate() {
        return this.mortalityRate;
    }

    public double getHealingRate() {
        return this.healingRate;
    }
    
    public int getSize() {
        return 3*Types.DOUBLE_SIZE;
    }

}
