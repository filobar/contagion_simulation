package model.virus;

import utils.size.Sized;

public interface Virus extends Sized {
    
    /**
     * @return
     *          The infection rate of the Virus.
     */
    double getInfectionRate();
    
    /**
     * @return
     *          The mortality rate of the Virus.
     */
    double getMortalityRate();
    
    /**
     * @return
     *          The healing rate of the Virus.
     */
    double getHealingRate();

}
