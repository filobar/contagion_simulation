package model.virus;

import utils.NotImplementedException;

public class BasicVirus extends AbstractVirus {
    
    public BasicVirus(final double infection, final double mortality, final double healing) {
        super(infection, mortality, healing);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(healingRate);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(infectionRate);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(mortalityRate);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BasicVirus other = (BasicVirus) obj;
        if (Double.doubleToLongBits(healingRate) != Double.doubleToLongBits(other.healingRate))
            return false;
        if (Double.doubleToLongBits(infectionRate) != Double.doubleToLongBits(other.infectionRate))
            return false;
        if (Double.doubleToLongBits(mortalityRate) != Double.doubleToLongBits(other.mortalityRate))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "BasicVirus [infectionRate=" + infectionRate + 
                         ", mortalityRate=" + mortalityRate + 
                         ", healingRate="   + healingRate + "]";
    }
    
    @Override
    public int getSize() {
        return super.getSize();
    }

}
