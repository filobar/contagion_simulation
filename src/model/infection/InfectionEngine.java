package model.infection;

import java.util.List;

import model.person.Person;
import model.virus.Virus;
import model.world.Territory;

public final class InfectionEngine {
    
    private final Virus virus;
    private final List<Person> people;
    private final Territory world;
    
    public InfectionEngine(final Virus virus, final List<Person> people, final Territory world) {
        super();
        this.virus = virus;
        this.people = people;
        this.world = world;
    }

}
