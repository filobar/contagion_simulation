package model.world;

import utils.size.Sized;
import utils.size.Types;

public abstract class AbstractWorld implements Territory, Sized {
    
    protected final long totalPopulation;
    protected long healthy;
    protected long infected;
    protected long healed;
    
    protected AbstractWorld(final long totalPopulation) {
        super();
        this.totalPopulation = totalPopulation;
    }
    
    public int getSize() {
        return 4 * Types.LONG_SIZE;
    }

}
