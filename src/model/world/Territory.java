package model.world;

import utils.size.Sized;

public interface Territory extends Sized {
    
    Long getTotalPopulation();
    
    Long getHealthy();
    
    Long getInfected();
    
    Long getHealed();
    
    //TODO check this method
    void infect(final long amount);

}
