package model.world;

import java.util.HashSet;
import java.util.Set;

import utils.size.Sized;
import utils.size.Types;

public final class BasicWorld extends AbstractWorld {
    
    private Set<Territory> countries = new HashSet<>();
    
    public BasicWorld(final long totalPopulation) {
        super(totalPopulation);
        this.healthy = totalPopulation;
        this.infected = 0;
        this.healed = 0;
    }

    public Long getTotalPopulation() {
        return this.totalPopulation;
    }

    public Long getHealthy() {
        return this.healthy;
    }

    public Long getInfected() {
        return this.infected;
    }

    public Long getHealed() {
        return this.healed;
    }

    public void infect(final long amount) {
        if(amount <= 0) {
            throw new IllegalArgumentException("Passed amount can't be zero or negative.");
        }
        if(amount > this.healthy) {
            throw new IllegalArgumentException("Passed amount can't be greater than healthy counter.");
        }
        
        this.infected += amount;
        this.healthy -= amount;
    }

    @Override
    public int getSize() {
        return super.getSize() +
                this.countries.stream().mapToInt(t -> t.getSize())
               .sum();
    }

}
