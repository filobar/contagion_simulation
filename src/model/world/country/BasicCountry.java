package model.world.country;

import model.climate.Climate;
import model.world.Territory;
import utils.size.Sized;
import utils.size.Types;

public class BasicCountry implements Territory, Sized {
    
    private final long totalPopulation;
    private long healthy;
    private long infected;
    private long healed;
    private final Climate climate;
    
    public BasicCountry(final long totalPopulation, final Climate climate) {
        super();
        this.totalPopulation = totalPopulation;
        this.healthy = totalPopulation;
        this.infected = 0;
        this.healed = 0;
        this.climate = climate;
    }

    public Long getTotalPopulation() {
        return this.totalPopulation;
    }

    public Long getHealthy() {
        return this.healthy;
    }

    public Long getInfected() {
        return this.infected;
    }

    public Long getHealed() {
        return this.healed;
    }

    public void infect(final long amount) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    public int getSize() {
        return 4*Types.LONG_SIZE + climate.getSize();
    }

}
