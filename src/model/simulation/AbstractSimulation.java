package model.simulation;

import model.virus.Virus;
import model.world.Territory;
import utils.size.Types;

public abstract class AbstractSimulation implements Simulation {
    
    protected int currentDay = 0;
    protected final Territory world;
    protected final Virus virus;
    
    protected AbstractSimulation(final Territory world, final Virus virus) {
        super();
        this.world = world;
        this.virus = virus;
    }
    
    public int getCurrentDay() {
        return this.currentDay;
    }
    
    public Territory getWorld() {
        return this.world;
    }
    
    public int getSize() {
        return Types.INT_SIZE +
                this.world.getSize() +
                this.virus.getSize();
    }

}
