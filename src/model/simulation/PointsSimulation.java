package model.simulation;

import model.virus.Virus;
import model.world.Territory;

public final class PointsSimulation extends AbstractSimulation {

    public PointsSimulation(final Territory world, final Virus virus) {
        super(world, virus);
        this.world.infect(1);
    }

    public void nextDay() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public int getSize() {
        return super.getSize();
    }

}
