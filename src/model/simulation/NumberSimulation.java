package model.simulation;

import model.virus.Virus;
import model.world.Territory;

public final class NumberSimulation extends AbstractSimulation {
    
    public NumberSimulation(final Territory world, final Virus virus) {
        super(world, virus);
        this.world.infect(1);
    }

    public void nextDay() {
        long h = this.world.getHealthy();
        if(h > 0) {
            long n = this.world.getInfected();
            long c = 0;
            for(long i=0; i<n && c<h; i++) {
                if(Math.random() <= this.virus.getInfectionRate()) {
                    c++;
                }
            }
            if(c != 0) {
                this.world.infect(c);
            }
        }
        this.currentDay++;
    }

    @Override
    public int getSize() {
        return super.getSize();
    }

}
