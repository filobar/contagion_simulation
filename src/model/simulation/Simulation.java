package model.simulation;

import model.world.Territory;
import utils.size.Sized;

public interface Simulation extends Sized {
    
    void nextDay();
    int getCurrentDay();
    
    Territory getWorld();

}
