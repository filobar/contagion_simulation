package model.person;

/**
 * Represents the different types of behavior of a {@link Person}.
 */
public enum PersonType {
    
    /**
     * Represents a {@link Person} that ages over time and,
     * because of this, his natural death's probability will grow over time.
     */
    AGING ("A person that ages over time and, " + 
            "because of this, his natural death's probability will grow over time."),
    
    /**
     * Represents a {@link Person} that tends to reproduce more than the others.
     */
    REPRODUCTIVE ("A person that tends to reproduce more than the others."),
    
    /**
     * Represents a {@link Person} that tends to interact more with other people
     * and, because of this, will spread faster the virus.
     */
    SOCIAL ("A person that tends to interact more with other people" + 
            " and, because of this, will spread faster the virus."),
    
    /**
     * Represents a {@link Person} that goes to work even in emergencies and in
     * quarantines.
     */
    WORKING ("A person that goes to work even in emergencies and in quarantines."),
    
    /**
     * Represents a {@link Person} that goes running outside or simply gets fit
     * (maybe by going in a gym) and, because of this, will spread faster the virus
     * until quarantine.
     */
    SPORTY ("A person that goes running outside or simply gets fit" + 
            " (maybe by going in a gym) and, because of this, will spread faster the virus" + 
            " until quarantine."),
    
    /**
     * Represents a {@link Person} that likes travelling around the world.
     * Because of this, will help the virus infecting new countries until
     * airports and docks (if any) are closed.
     */
    TRAVELER ("A person that likes travelling around the world." + 
            " Because of this, will help the virus infecting new countries until" + 
            " airports and docks (if any) are closed.");
    
    private String description;
    
    private PersonType(final String description) {
        this.description = description;
    }
    
    /**
     * @return
     *          A {@link String} with a short description of the enum value.
     */
    public String description() {
        return this.description;
    }

}
