package model.person;

import utils.size.Sized;

public interface Person extends Sized {
    
    /**
     * Tells the Person to move, updating its current position and generating
     * a new direction.
     */
    void updatePosition();
    
    /**
     * Tells the Person to calculate his/her probability to get infected.
     * 
     * @return
     *          A double between 0 and 1, both inclusive, representing that probability.
     */
    double getP();
    
    

}
