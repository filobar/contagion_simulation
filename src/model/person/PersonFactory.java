package model.person;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import utils.Point2D;

/**
 * An interface with Factory pattern for easily creating {@link Person} instances.
 */
public interface PersonFactory {
    
    /**
     * @return
     *          A {@link Person} with sex set to 'MALE'.
     *          Position and direction are random.
     */
    static Person male() {
        return new BasicPerson(Point2D.random(), Point2D.random(), Sex.MALE, null);
    }
    
    /**
     * @param amount
     *          The number of random males requested.
     *          
     * @return
     *          A List of given length, filled with males.
     *          Positions and direction are random.
     */
    static List<Person> males(final int amount){
        final List<Person> l = new LinkedList<>();
        Stream.generate(() -> 1)
                .limit(amount)
                .forEach(a -> l.add(male()));
        return l;
    }
    
    /**
     * @return
     *          A {@link Person} with sex set to 'FEMALE'.
     *          Position and direction are random.
     */
    static Person female() {
        return new BasicPerson(Point2D.random(), Point2D.random(), Sex.FEMALE, null);
    }
    
    /**
     * @param amount
     *          The number of random females requested.
     *          
     * @return
     *          A List of given length, filled with females.
     *          Positions and direction are random.
     */
    static List<Person> females(final int amount){
        final List<Person> l = new LinkedList<>();
        Stream.generate(() -> 1)
                .limit(amount)
                .forEach(a -> l.add(female()));
        return l;
    }

}
