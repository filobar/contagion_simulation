package model.person;

import java.util.Set;

import utils.Point2D;

public final class BasicPerson extends AbstractPerson {

    public BasicPerson(final Point2D position, final Point2D direction, final Sex sex, final Set<PersonType> behavior) {
        super(position, direction, sex);
    }

    public void updatePosition() {
        this.position = Point2D.add(this.position, this.direction);
        this.direction = Point2D.random();
    }

    //TODO check this method
    public double getP() {
        return 1;
    }

    @Override
    public int getSize() {
        // TODO Auto-generated method stub
        return 0;
    }

}
