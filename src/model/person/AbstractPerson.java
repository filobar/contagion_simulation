package model.person;

import utils.Point2D;
import utils.size.Types;

public abstract class AbstractPerson implements Person {
    
    protected Point2D position;
    protected Point2D direction;
    protected final Sex sex;
    protected int age;
    
    /**
     * Normal constructor that initializes each field.
     * 
     * @param position
     *          The starting (and current) position of this person.
     *          
     * @param direction
     *          The starting (and current) movement of this person.
     *          
     * @param sex
     *          The sex of this person.
     */
    protected AbstractPerson(final Point2D position, final Point2D direction, final Sex sex) {
        super();
        this.position = position;
        this.direction = direction;
        this.sex = sex;
    }
    
    /**
     * Constructor that requires only the {@link Sex}
     * Position and direction are initialized at random.
     * 
     * @param sex
     *          The sex of this person.
     */
    protected AbstractPerson(final Sex sex) {
        this(Point2D.random(), Point2D.random(), sex);
    }
    
    public int getSize() {
        return 2*position.getSize() +
                2*Types.INT_SIZE;
    }

}
