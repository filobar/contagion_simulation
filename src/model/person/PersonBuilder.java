package model.person;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import utils.Point2D;

//TODO check this class (is it useful?)
//TODO add javadoc
//TODO is it needed to be built just once?
public final class PersonBuilder {
    
    private Sex sex;
    private final Set<PersonType> behavior;
    private Optional<Point2D> position;
    private Optional<Point2D> direction;
    
    private boolean alreadyBuilt;
    
    public PersonBuilder() {
        super();
        this.sex = null;
        this.behavior = new HashSet<>();
        this.position = Optional.empty();
        this.direction = Optional.empty();
        this.alreadyBuilt = false;
    }
    
    public PersonBuilder sex(final Sex sex) {
        this.sex = Objects.requireNonNull(sex);
        return this;
    }
    
    public PersonBuilder type(final PersonType type) {
        this.behavior.add(Objects.requireNonNull(type));
        return this;
    }
    
    public PersonBuilder position(final Point2D position) {
        this.position = Optional.of(Objects.requireNonNull(position));
        return this;
    }
    
    public PersonBuilder direction(final Point2D direction) {
        this.direction = Optional.of(Objects.requireNonNull(direction));
        return this;
    }
    
    public Person build() {
        if(this.alreadyBuilt) {
            throw new IllegalStateException("Can't build same Person twice.");
        }
        if(this.sex == null) {
            throw new IllegalStateException("Sex can't be null.");
        }
        
        this.alreadyBuilt = true;
        
        if(this.position.isEmpty()) {
            this.position = Optional.of(Point2D.random());
        }
        if(this.direction.isEmpty()) {
            this.direction = Optional.of(Point2D.random());
        }
        
        return new BasicPerson(position.get(), direction.get(), sex, behavior);
    }

}
