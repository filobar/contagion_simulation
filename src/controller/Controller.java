package controller;

import model.simulation.Simulation;

public interface Controller {
    
    Simulation getNewSimulation();

}
