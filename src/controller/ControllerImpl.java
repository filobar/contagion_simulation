package controller;

import model.simulation.NumberSimulation;
import model.simulation.Simulation;
import model.virus.BasicVirus;
import model.world.BasicWorld;
import view.gui.MainMenu;

public final class ControllerImpl implements Controller {
    
    private static Controller controller = null;
    
    /**
     * Allocates a new MainMenu.
     * Parameter args ignored.
     */
    public static void main(String[] args) {
        new MainMenu();
    }
    
    private ControllerImpl() {
        super();
    }
    
    public static Controller getInstance() {
        if(ControllerImpl.controller == null) {
            ControllerImpl.controller = new ControllerImpl();
        }
        return ControllerImpl.controller;
    }

    public Simulation getNewSimulation() {
        return new NumberSimulation(new BasicWorld(1000), new BasicVirus(0.1, 0.1, 0.1));
    }

}
